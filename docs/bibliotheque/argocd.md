# ARGOCD

En plus des manifests spécifique à ArgoCD il y a aussi un utilitaire en ligne de commande pour argocd.

Cette utilitaire complète les commandes que l'on peut passer avec kubectl :

- Il ajoute des commandes impératives sur les `Applications` (tel que la création d'Apps sans passer par un descripteur, ...)
- Il permet des opérations "admin" tel que changer le mot de passe, ....
- ....

Certaines de ces commandes sont utilisées par le script d'installation de ArgoCD.

**Ce qui est fait avec l'utilitaire en ligne de commande peut aussi être fait via l'interface web de argocd**

## Commandes de base

Seul les commandes de bases seront décrites, pour voir les autres commandes `argocd --help`.  
Dans la mesure où nous n'utiliserons pas un certificat TLS valide pour accéder à argocd (une url en argocd.$IP.sslip.io avec IP : IP du loadbalancer du cluster) notre connexion (via argocd login qui est fait dans le script d'installation) utilise l'option `--insecure`.

### Liste des `Applications`

```shell
argocd app list
```

Ou en utilisant kubectl

```shell
kubectl get applications.argoproj.io -A
```

### Détail d'une `Application`

```shell
argocd app get NOM_DE_LAPPLICATION_ARGO_CD
```

Vous verez le détail et le status de l'Application Argo

### Synchroniser une `Application`

```shell
argocd app sync NOM_DE_LAPPLICATION_ARGO_CD
```

Permet de demander sans attendre la synchronisation d'une application entre son état définit dans le repository git et l'état tel que déployé.  
C'est utile pour ce TP, car les `Applications` sont synchronisées toutes les 3 minutes.

Il y a plus d'options disponibles avec `sync` comme `--async`, ... pour connaître ces flags `argcod app sync --help`

TIPS: Force la synchronisation toutes les applications

```shell
argocd app list -o name | argocd app list -o name
```

### Supprimer une `Application`

En cas de besoin vous pouvez supprimer une application ArgoCD

```shell
argocd app sync NOM_DE_LAPPLICATION_ARGO_CD
```

Ou en utilisant kubectl

```shell
kubectl delete applications.argoproj.io NOM_DE_LAPPLICATION_ARGOCD -n NAMESPACE_DE_LAPPLICATION_ARGOCD
```

(ou encore via un `kubectl delete -f FICHIER_MANIFEST_ARGO_APS ...`)
